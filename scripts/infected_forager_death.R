####################################################################################
# Title:    Script end project - Effect of forager death rates script
# By:       Nadia Choudhury 
# Stdnrs:   390878
# Date:     19-08-2020
####################################################################################
# This script is also split up in two parts. The first part of the script construct 
# altered versions of several functions of the script 'base_model.R'. These
# functions are 'parameters', 'add_matrix', and 'calc_period'. 
# The second part of the script creates two pdf files. The first pdf shows how the
# effect of increasing forager death rates influences the honey yield. The second
# pdf contains a graph that shows the effect of various forager death rates
# upon the annual honey yield. 
####################################################################################
# This script requires the script base_model.R
####################################################################################
source(file.path(getwd(), 'scripts', 'base_model.R'))

####################################################################################
# Construct altered functions of the model
####################################################################################

# Alternative parameter function with death rate as argument
parameters1 <- function(season, tilde_alpha_v, gamma_omega_v, phi_1_v){
    # Fixed parameter values
    fixed_para <- c(a = 0.004,                     b = 0.008,
                    h = 100,                       v = 2,
                    sigma = 1.5,                   lambda = 10000,
                    n = 2,                         phi_1 = phi_1_v,
                    fh = 100)
    
    # Same parameter values for the seaons spring, summer, harvest
    three_para <- c(psi_min = psi_min_s[1],        psi_max = psi_max_s[1],
                    eta_0 = eta_0_s[1],            hat_eta_0 = hat_eta_0_s[1],
                    eta_1 = eta_1_s[1],            hat_eta_1 = hat_eta_1_s[1],
                    phi_0 = phi_0_s[1],            hat_phi_0 = phi_0_s[1],
                    hat_phi_1 = phi_1_s[1],
                    theta_0 = 0.007 * omega_s[1],  theta_1 = 0.014 * omega_s[1],
                    theta_b = theta_b_s[1],
                    tilde_alpha = tilde_alpha_v * tilde_alpha_s[1])
    
    # Parameter values of spring season
    if (season == 1){
        para <- c(beta = beta_s[1],              k = k_s[1],
                  alpha = alpha_s[1],            gamma = gamma_s[1] * gamma_omega_v,
                  delta = delta_s[1],   
                  c_0 = c_0_s[1],                c_1 = c_1_s[1],
                  mu = mu_s[1],                  ha = ha_s[1],
                  fixed_para,                    three_para
        )}
    
    # Parameter values of summer season
    if (season == 2){
        para <- c(beta = beta_s[2],              k = k_s[2],
                  alpha = alpha_s[2],            gamma = gamma_s[2] * gamma_omega_v,
                  delta = delta_s[2],
                  c_0 = c_0_s[2],                c_1 = c_1_s[2],
                  mu = mu_s[1],                  ha = ha_s[1],
                  fixed_para,                    three_para
        )}
    
    # fall season
    if (season == 3){
        para <- c(beta = beta_s[3],              k = k_s[3],
                  alpha = alpha_s[3],            gamma = gamma_s[3] * gamma_omega_v,
                  delta = delta_s[3],
                  c_0 = c_0_s[3],                c_1 = c_1_s[3],
                  mu = mu_s[1],                  ha = ha_s[1],
                  fixed_para,                    three_para
        )}
    
    # Parameter values of harvest day of fall
    if (season == 4){
        para <- c(beta = beta_s[3],              k = k_s[3],
                  alpha = alpha_s[3],            gamma = gamma_s[3] * gamma_omega_v,
                  delta = delta_s[3],
                  c_0 = c_0_s[3],                c_1 = c_1_s[3],
                  mu = mu_s[2],                  ha = ha_s[2],
                  fixed_para,                    three_para
        )}
    
    # Parameter values of winter
    if (season == 5){
        para <- c(beta = beta_s[4],              k = k_s[4],
                  alpha = alpha_s[4],            gamma = gamma_s[4] * gamma_omega_v,
                  delta = delta_s[4],
                  c_0 = c_0_s[4],                c_1 = c_1_s[4],
                  mu = mu_s[1],                  ha = ha_s[1],
                  psi_min = psi_min_s[2],        psi_max = psi_max_s[2], 
                  eta_0 = eta_0_s[2],            hat_eta_0 = hat_eta_0_s[2], 
                  eta_1 = eta_1_s[2],            hat_eta_1 = hat_eta_1_s[2],
                  phi_0 = phi_0_s[2],            hat_phi_0 = phi_0_s[2],
                  hat_phi_1 = phi_1_s[2],
                  theta_0 = 0.007 * omega_s[2],  theta_1 = 0.014 * omega_s[2],
                  theta_b = theta_b_s[2],
                  tilde_alpha = tilde_alpha_v * tilde_alpha_s[2],
                  fixed_para
        )}
    return(para)
}

# Solve the differential equations and add the results to an existing matrix
add_matrix1 <- function(mtr, t1, t2, s_nr, tilde_alpha, gamma_omega, phi_1){
    if (s_nr != 4){
        res <- ode(times = times[t1:t2], y = mtr[t1, 2:11], func = hb_colony,
                   parms = parameters1(season = s_nr, tilde_alpha_v = tilde_alpha, 
                                       gamma_omega_v = gamma_omega, phi_1_v = phi_1))
        # Round up, because there isn't something like a 0.34 bee
        res[, c(2:5, 9:11)] <- round(res[, c(2:5, 9:11)], 0)
        # If values are less than 10e-6 then set it to 0
        res[res < 10E-06] <- 0}
    else{
        # Harvest event (only lasts for one day)
        harvest_event <- data.frame(var = "s", time = t2, value = 13000, method = "add")
        res <- ode(times = times[t1:t2], y = mtr[t1, 2:11], func = hb_colony,
                   parms = parameters1(season = s_nr, tilde_alpha_v = tilde_alpha, 
                                       gamma_omega_v = gamma_omega, phi_1_v = phi_1),
                   events = list(data = harvest_event))
    }
    rbind(mtr, res)[-t1,]
}

# Alternative parameter function with death rate as argument
calc_period1 <- function(tilde_alpha, gamma_omega, years, disease = T, phi_1){
    # It's important that year 1 = 0 for the amount of years in days calculation
    years <- years - 1
    # Constructing a matrix were we can build upon
    res <- as.matrix(ode(times = times[1:92], y = state, func = hb_colony, 
                         parms = parameters1(season = 1, tilde_alpha_v = tilde_alpha,
                                             gamma_omega_v = gamma_omega, phi_1_v = phi_1),
                         maxsteps = 1e5))
    res[, c(2:5, 9:11)] <- round(res[, c(2:5, 9:11)], 0)
    # If values are less than 10e-6 then set it to 0
    res[res < 10E-06] <- 0
    # Run for a x amount of years
    for (i in 0:years){
        days <- i * 365
        if (i != 0){
            res <- add_matrix1(mtr = res, t1 = 1 + days, t2 = 92 + days, s_nr = 1,
                               tilde_alpha = tilde_alpha, gamma_omega = gamma_omega,
                               phi_1 = phi_1)
        }
        res <- add_matrix1(mtr = res, t1 = 92 + days, t2 = 183 + days, s_nr = 2,
                           tilde_alpha = tilde_alpha, gamma_omega = gamma_omega,
                           phi_1 = phi_1)
        res <- add_matrix1(mtr = res, t1 = 183 + days, t2 = 273 + days, s_nr = 3,
                           tilde_alpha = tilde_alpha, gamma_omega = gamma_omega,
                           phi_1 = phi_1)
        res <- add_matrix1(mtr = res, t1 = 273 + days, t2 = 274 + days, s_nr = 4,
                           tilde_alpha = tilde_alpha, gamma_omega = gamma_omega,
                           phi_1 = phi_1)
        if (i == 0 && disease == T){
            res[274, 2] <- res[274, 2] - 10
            res[274, 3] <- res[274, 3] + 10
        }
        res <- add_matrix1(mtr = res, t1 = 274 + days, t2 = 365 + days + 1, s_nr = 5,
                           tilde_alpha = tilde_alpha, gamma_omega = gamma_omega,
                           phi_1 = phi_1)
    }
    return(res)
}

####################################################################################
# End of altered model
####################################################################################

# Function to calculate the total honey yield with various death rates
death_effect <- function(go, ta, dis = T, dif = F){
    death_date1 <- vector()
    death_date2 <- vector()
    if (dif == T){
        death_date1 <- append(death_date1, rep(NA, length(death_values) - 
                                                   length(death_values2)))
        death_date2 <- append(death_date2, rep(NA, length(death_values) - 
                                                   length(death_values2)))
        for (v in death_values2){
            death_date1 <- append(death_date1, sum(calc_period1(
                gamma_omega = go, tilde_alpha = ta, years = 10, phi_1 = v, 
                disease = dis)[seq(273, 365 * 10, 365),7]))
            death_date2 <- append(death_date2, sum(calc_period1(
                gamma_omega = go, tilde_alpha = ta, years = 10, phi_1 = v,
                disease = dis)[seq(274, 365 * 10, 365),7]))
        }
    } else {
        for (v in death_values){
            death_date1 <- append(death_date1, sum(calc_period1(
                gamma_omega = go, tilde_alpha = ta, years = 10, phi_1 = v, 
                disease = dis)[seq(273, 365 * 10, 365),7]))
            death_date2 <- append(death_date2, sum(calc_period1(
                gamma_omega = go, tilde_alpha = ta, years = 10, phi_1 = v,
                disease = dis)[seq(274, 365 * 10, 365),7]))
            }
        }
    death_res <- death_date1 - death_date2
    return(death_res)
}

# Function to calculate the honey yield for each year
annual_honey_yield <- function(res, year){
    date1 <- seq(273, 365 * year, 365)
    date2 <- seq(274, 365 * year, 365)
    honey <- res[date1, 7] - res[date2, 7]
    return(honey)
}

# Vectors of different death rate values
death_values <- seq(0.08511, 0.34044, 0.0042555)
death_values2 <- seq(0.2383080, 0.34044, 0.0042555)
death_values3 <- c(0.08511, 0.127665, 0.1404315, 0.17222,
                   0.212775, 0.25533, 0.297885, 0.34044)

# Calculate the total honey yield in various cases
dis_free <- death_effect(go = 0, ta = 0, dis = F)
inf1 <- death_effect(go = 0.185, ta = 0.13, dif = T)
inf2 <- death_effect(go = 0.14, ta = 0.12)
inf3 <- death_effect(go = 0.13, ta = 0.13)
inf4 <- death_effect(go = 0.12, ta = 0.14)
inf5 <- death_effect(go = 0.13, ta = 0.166)

# Calculate annual honey yield for various forager death rates
res1 <- calc_period1(tilde_alpha = 0.13, gamma_omega = 0.166,
                     years = 10, phi_1 = death_values3[1])
res2 <- calc_period1(tilde_alpha = 0.13, gamma_omega = 0.166,
                     years = 10, phi_1 = death_values3[2])
res3 <- calc_period1(tilde_alpha = 0.13, gamma_omega = 0.166,
                     years = 10, phi_1 = death_values3[3])
res4 <- calc_period1(tilde_alpha = 0.13, gamma_omega = 0.166,
                     years = 10, phi_1 = death_values3[4])
res5 <- calc_period1(tilde_alpha = 0.13, gamma_omega = 0.166,
                     years = 10, phi_1 = death_values3[5])
res6 <- calc_period1(tilde_alpha = 0.13, gamma_omega = 0.166,
                     years = 10, phi_1 = death_values3[6])
res7 <- calc_period1(tilde_alpha = 0.13, gamma_omega = 0.166,
                     years = 10, phi_1 = death_values3[7])
res8 <- calc_period1(tilde_alpha = 0.13, gamma_omega = 0.166,
                     years = 10, phi_1 = death_values3[8])

# Plot the graph of the total honey yield in various cases
pdf(file.path(getwd(), 'output', 'infected_yield.pdf'), width = 10, height = 7)
# Move labels  a bit closer to the axes
par(mgp = c(2.5,1,0))
plot(dis_free ~ death_values, axes = F, ylim = c(50000, 350000), type = 'b', lty = 2,
     main = "Effect of increasing infected foragers death rates in various cases",
     ylab = expression(paste('Honey [gr x ', 10^4, ']')), 
     xlab = bquote(phi[1] ~ '(death rate values)'))
lines(inf1 ~ death_values, type = 'b', lty = 2, col = 'steelblue')
lines(inf2 ~ death_values, type = 'b', lty = 2, col = 'darkorchid2')
lines(inf3 ~ death_values, type = 'b', lty = 2, col = 'green')
lines(inf4 ~ death_values, type = 'b', lty = 2, col = 'orange')
lines(inf5 ~ death_values, type = 'b', lty = 2, col = 'grey')
axis(1, at = seq(0.08511, 0.34044, 0.017022), 
     labels = signif(seq(0.08511, 0.34044, 0.017022), 2))
axis(2, at = seq(50000, 350000, by = 50000), labels = seq(5, 35, by = 5), las = 1)
mtext(expression(10^4), adj = 0, padj = -0.5, outer = F, cex = 0.6)
legend('bottomright', lty = 2, cex = 0.9, bty = 'n', pch = 1,
       col = c('black', 'steelblue', 'darkorchid2', 'green', 'orange', 'grey'),
       legend = sapply(c(bquote('disease-free:' ~ tilde(alpha) == 0 ~ ',' 
                                ~ gamma[omega] == 0),
                         bquote('fade out:' ~ tilde(alpha) == 0.185 ~ ',' 
                                ~ gamma[omega] == 0.13),
                         bquote('co-existence:' ~ tilde(alpha) == 0.14 ~ ',' 
                                ~ gamma[omega] == 0.12),
                         bquote('co-existence:' ~ tilde(alpha) == 0.13 ~ ',' 
                                ~ gamma[omega] == 0.13),
                         bquote('co-existence:' ~ tilde(alpha) == 0.12 ~ ',' 
                                ~ gamma[omega] == 0.14),
                         bquote('colony failure:' ~ tilde(alpha) == 0.13 ~ ',' 
                                ~ gamma[omega] == 0.166)),
                       as.expression))
dev.off()

# Plot the graph of annual honey yield for various forager death rates
pdf(file.path(getwd(),  'output', 'infected_annual_honey.pdf'), width = 10, height = 7)
plot(annual_honey_yield(res1, 10) ~ year_marks, type = 'b',
     main = 'Honey yield with various infected foragers death rates',
     xlab = 'Time [years]', ylab = 'Honey [kilograms]',
     ylim = c(0, 35000), axes = F)
lines(annual_honey_yield(res2, 10) ~ year_marks, type = 'b', lty = 2, col = 'darkorchid2')
lines(annual_honey_yield(res3, 10) ~ year_marks, type = 'b', lty = 2, col = 'tomato')
lines(annual_honey_yield(res4, 10) ~ year_marks, type = 'b', lty = 2, col = 'green')
lines(annual_honey_yield(res5, 10) ~ year_marks, type = 'b', lty = 2, col = 'steelblue')
lines(annual_honey_yield(res6, 10) ~ year_marks, type = 'b', lty = 2, col = 'lightblue')
lines(annual_honey_yield(res7, 10) ~ year_marks, type = 'b', lty = 2, col = 'grey')
lines(annual_honey_yield(res8, 10) ~ year_marks, type = 'b', lty = 2, col = 'green4')
axis(1, at = year_marks, labels = seq(1, 10, by = 1))
axis(2, at = seq(0, 35000, by = 5000), labels = seq(0, 35, by = 5), las = 1)
box(bty = 'l')
legend('bottomleft', cex = 0.8, bty = 'n', lty = c(1, rep(2, 7)), pch = 1,
       col = c('black', 'darkorchid2', 'tomato', 'green',
               'steelblue', 'lightblue', 'grey', 'darkgreen'),
       legend = sapply(c(bquote(phi[1] == .(death_values3[1])),
                         bquote(phi[1] == .(death_values3[2])),
                         bquote(phi[1] == .(death_values3[3])),
                         bquote(phi[1] == .(death_values3[4])),
                         bquote(phi[1] == .(death_values3[5])),
                         bquote(phi[1] == .(death_values3[6])),
                         bquote(phi[1] == .(death_values3[7])),
                         bquote(phi[1] == .(death_values3[8]))),
                       as.expression))
dev.off()

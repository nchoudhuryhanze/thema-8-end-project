# README 

Author: Nadia Choudhury  
Student number: 390878  
Date: 28 - 08 - 20  

This is a research project about the mathematical model of a honey bee colony with Nosema ceranae parasite.

## Files

- scripts  
|- base_model.R  
|- base_simulations.R  
|- population_reduction.R  
|- honey_reduction.R  
|- infected_forager_death.R  
- output  
|- no_disease.pdf  
|- colony_failure.pdf  
|- co_existence.pdf  
|- fade_out.pdf  
|- honey_reduction.pdf  
|- population_reduction.pdf  
|- infeceted_annual_honey.pdf  
|- infected_yield.pdf  
- misc  
|- beehive.png  
|- diagram.png  
|- Comper_thesis.pdf  
- README.md (this file)  
- endproject_nadia.Rmarkdown  
- endproject_nadia.pdf  

## Pre-requisitions

To run these the  R package Desolve is required. These scripts were written in R vesion 4.0.1. 

## Usage

Before running a script make sure to set the working directory to the correct path.

```R
setwd('path/to/directory/thema-8-end-project/')
```
The output of the R scripts appears in the output folder. 